﻿// Learn more about F# at http://fsharp.net
// See the 'F# Tutorial' project for more help.
open System
open System.Net
open System.Text
open Suave
open Suave.Filters
open Suave.Operators
open Suave.Successful
open Suave.Writers

let app = 
  choose [
    GET 
    >=> path "/" 
    >=> context (fun ctx ->
                  let sb = 
                    (new StringBuilder())
                      .AppendFormat("Request Url: {0}", ctx.request.url)
                      .AppendFormat("Request Url: {0}", ctx.request.url)
                      .AppendFormat("Headers: {0}", sprintf "%A" ctx.request.headers)
                  printfn "%s" <| sb.ToString()
                  OK <| "<pre>" + sb.ToString() + "</pre>"
                ) 
    ]

[<EntryPoint>]
let main argv = 
    let port = Sockets.Port.Parse "8091"
    startWebServer 
      { defaultConfig with 
          bindings = [ Http.HttpBinding.mk HTTP IPAddress.Any port ] } 
      app
    0 // return an integer exit code

